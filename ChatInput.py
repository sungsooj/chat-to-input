import tornado.ioloop
import tornado.web
import tornado.websocket
import win32com.client
import json
shell = win32com.client.Dispatch("WScript.Shell")
shell.AppActivate('VisualBoyAdvance')

clients = []

class IndexHandler(tornado.web.RequestHandler):
  @tornado.web.asynchronous
  def get(request):
    request.render("index.html")

class WebSocketChatHandler(tornado.websocket.WebSocketHandler):
  def open(self, *args):
    print("open", "WebSocketChatHandler")
    clients.append(self)

  def on_message(self, message):        
    print message
    shell.AppActivate('VisualBoyAdvance')
    t = json.loads(message)
    print(t['message'])
    if t['message'] == 'start':
        for x in range(0, 200):
            shell.SendKeys('{ENTER}')
    elif t['message'] == 'a':
        for x in range(0, 200):
            shell.SendKeys('{z}')
    elif t['message'] == 'b':
        for x in range(0, 200):
            shell.SendKeys('{x}')
    elif t['message'] == 'up':
        for x in range(0, 200):
            shell.SendKeys('{i}')
    elif t['message'] == 'down':
        for x in range(0, 200):
            shell.SendKeys('{k}')
    elif t['message'] == 'left':
        for x in range(0, 200):
            shell.SendKeys('{j}')
    elif t['message'] == 'right':
        for x in range(0, 200):
            shell.SendKeys('{l}')               
    for client in clients:
        client.write_message(message)
        
  def on_close(self):
    clients.remove(self)

app = tornado.web.Application([(r'/chat', WebSocketChatHandler), (r'/', IndexHandler)])

app.listen(8888)
tornado.ioloop.IOLoop.instance().start()
